let trainer = {
	name: 'Ash Ketchum',
	age: '10',
	pokemon: ['snorlax', 'charizardDragon', 'onyxSteelix','ninetails'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log("Hello my name is " + this.name + ' ' + this.age);
	}
}
console.log(trainer);


function pokemonTrainer(name, pokemon) {
	//"This" keyword allows us to get the value of the information
	this.name = name;
	this.pokemon = pokemon;
};

let pokemonOwner = new pokemonTrainer('Ash Ketchum');
console.log("Result from dot notation: ");
console.log(pokemonOwner.name);


function Pokemons(name, level, health, attack) {
	//"This" keyword allows us to get the value of the information
	this.name = name;
	// this.pokemon2 = pokemon2;
	// this.pokemon3 = onyxSteelix;
	// this.pokemon4 = ninetails;
};

let pokemonGen = new Pokemons('onyxSteelix','ninetails');

let pokemonGen1 = new Pokemons('snorlax', 'charizardDragon');

let array = [pokemonGen, pokemonGen1];
console.log(array);







// let pokemon1 = new pokemons ('snorlax', 'charizardDragon', 'onyx/steelix','ninetails'),
// // console.log('Result from square bracket notation:');
// let array = [pokemon1, pokemons];
// console.log(array);




// function Pokemon(name, level){

// 	// Properties
// 	this.name = name;
// 	this.level = level;
// 	this.health = 2 * level;
// 	this.attack = level;

// 	// Methods
// 	this.tackle = function(target){
// 		console.log(this.name + ' tackled ' + target.name);
// 		console.log("targetPokemon's health is now reduced to " + Number(target.health - this.attack));
// 	};
// 	this.faint = function(){
// 		console.log(this.name + ' fainted. ');
// 	};

// }